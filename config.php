<?php return array (
  'Effects' => 
  array (
    'api.EffectResize' => 
    array (
      'name' => 'Effect Resize',
      'visible' => true,
    ),
    'api.EffectBlur' => 
    array (
      'name' => 'Efect Blur',
      'visible' => false,
    ),
    'api.EffectRotate' => 
    array (
      'name' => 'Efect Rotate',
      'visible' => true,
    ),
    'plugins.EffectSharpen' => 
    array (
      'name' => 'Effect Sharpen',
      'visible' => true,
    ),
    'plugins.Effect6' => 
    array (
      'name' => 'Efect 6',
      'visible' => false,
    ),
    'plugins.Effect7' => 
    array (
      'name' => 'Efect 7',
      'visible' => false,
    ),
  ),
);