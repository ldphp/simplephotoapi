<?php
namespace ImageApi;
interface IEffect
{ 
 /**
  * Set the image instance.
  * 
  * @param mixed $image 
  * @access public
  * @return void
  */
 public function setImage($image);

/**
  * Get the image instance.
  * 
  * @access public
  * @return _image Returns the image instance.
  */
 public function getImage();

 /**
  * Get the effect name. 
  * 
  * @access public
  * @return void
  */
 public function getName();

 /**
  * Set the name of the effect. 
  * 
  * @param mixed $name 
  * @access public
  * @return void
  */
 public function setName($name);

 /**
  * The implementation of the apply effect function.
  * 
  * @param array $params 
  * @abstract
  * @access public
  * @return void
  */

}
