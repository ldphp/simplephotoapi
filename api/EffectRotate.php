<?php
namespace ImageApi;
require_once 'AEffect.php';
require_once 'IEffect.php';
class EffectRotate extends AEffect implements IEffect{
   
 /**
  * The resize effect imprementation. 
  * 
  * @param array $params 
  * @access public
  * @return void
  */
 public function applyEfect($params = array()){
    $degrees = $params['degrees'];

     // Transparent black will be used as the background for the uncovered region
     $transparent = imagecolorallocatealpha($this->_image, 0, 0, 0, 127);

     // Rotate, setting the transparent color
     $image = imagerotate($this->_image, 360 - $degrees, $transparent, 1);

     // Save the alpha of the rotated image
     imagesavealpha($image, TRUE);

     // Get the width and height of the rotated image
     $width  = imagesx($image);
     $height = imagesy($image);

     if (imagecopymerge($this->_image, $image, 0, 0, 0, 0, $width, $height, 100))
     {
         // Swap the new image for the old one
         imagedestroy($this->_image);
         $this->_image = $image;
     }
     return $this->_image;
 }
}
