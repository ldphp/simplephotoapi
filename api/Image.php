<?php
namespace ImageApi;
/**
 * The image loading class.
 * 
 * @package Image
 * @version 1
 * @copyright 2011
 * @author Liviu Dobre 
 */
class Image
{	
    /**
     * Full path of the file.
     * 
     * @var string
     * @access protected
     */
	protected $_file;

    /**
     * The width of the file. 
     * 
     * @var mixed
     * @access protected
     */
    protected $_width;

    /**
     * The height of the file.
     * 
     * @var mixed
     * @access protected
     */
    protected $_height;

    /**
     * The extension of the file. 
     * 
     * @var mixed
     * @access protected
     */
    protected $_type;

    /**
     * The mime type of the file. 
     * 
     * @var mixed
     * @access protected
     */
    protected $_mime;


    /**
     * The loaded image instance. 
     * 
     * @var mixed
     * @access protected
     */
	protected $_image;

    /**
     * The GD function used to load the file.
     * 
     * @var mixed
     * @access protected
     */
    protected $_create_function;

    /**
     * The effects that must be applied to the image.
     * 
     * @var mixed
     * @access protected
     */
	protected $_efects;
	
    /**
     * The image construct. 
     * 
     * @param mixed $image_path 
     * @access public
     * @return void
     */
    public function __construct($image_path){
        try
		{
			// Get the real path to the file
			$file = realpath($image_path);

			// Get the image information
			$info = getimagesize($file);
		}
		catch (Exception $e)
		{
			// Ignore all errors while reading the image
		}

		if (empty($file) OR empty($info))
		{
			throw new Exception('Not an image or invalid image.');
		}

		// Store the image information
		$this->_file   = $file;
		$this->_width  = $info[0];
		$this->_height = $info[1];
		$this->_type   = $info[2];
		$this->_mime   = image_type_to_mime_type($this->type);
		
        $this->loadImage($file);
	}
	
    /**
     * The function that loads the image from disk. 
     * 
     * @param string $image_path 
     * @access public
     * @return void
     */
    final private function loadImage($image_path){
        switch ($this->type)
		{
			case IMAGETYPE_JPEG:
				$create = 'imagecreatefromjpeg';
			break;
			case IMAGETYPE_GIF:
				$create = 'imagecreatefromgif';
			break;
			case IMAGETYPE_PNG:
				$create = 'imagecreatefrompng';
			break;
		}

		if ( ! isset($create) OR ! function_exists($create)) {
			throw new Exception('Installed GD does not support all the functions needed.');
        }

		// Save function for future use
		$this->_create_function = $create;

		// Save filename for lazy loading
		$this->_image = $this->file;
	}
	
    /**
     * Returns the image instance. 
     * 
     * @access public
     * @return void
     */
    public function getImage(){
	    return $this->_image;	
	}

    /**
     * Add the effect to effects queue. 
     * 
     * @param AEfect $efect 
     * @access public
     * @return void
     */
	public function addEfect(AEffect $efect){
		$this->_efects[$efect->getName()] = $efect;
	}

    /**
     * Applies the effects to the image. 
     * 
     * @access public
     * @return void
     */
    public function applyEfects(){
		foreach($this->_efects as $efect){
			$efect->setImage(
            $this->_image, array(
                'width' => $this->_width,
                'height' => $this->_height
            ));
			$this->_image = $efect->applyEfect();
		}
	}
	public function save($path_file_name){
		file_put_contents($path_file_name, imagejpeg($this->_image));
	}
	public function __destruct() {
       imagedestroy($this->_image);
   }
}
