<?php
namespace ImageApi;
require_once 'IEffect.php';
abstract class AEffect implements IEffect
{ 
 /**
  *  The image instance.
  * 
  * @var mixed
  * @access protected
  */
 protected $_image;

 /**
  * The name of the effect.
  * 
  * @var mixed
  * @access protected
  */
 protected $_name;

 /**
  * The effect parameters
  * 
  * @var array
  * @access protected
  */
 protected $_params = array();
 
 /**
  * Set the image instance.
  * 
  * @param mixed $image 
  * @access public
  * @return void
  */
 public function setImage($image, $params = array()){
   $this->_image = $image;
 }

 /**
  * Get the image instance.
  * 
  * @access public
  * @return _image Returns the image instance.
  */
 public function getImage(){
  return $this->_image;
 }

 /**
  * Get the effect name. 
  * 
  * @access public
  * @return void
  */
 public function getName(){
  return $this->_name;
 }

 /**
  * Set the name of the effect. 
  * 
  * @param mixed $name 
  * @access public
  * @return void
  */
 public function setName($name){
  $this->_name = $name;
 }

 /**
  * The implementation of the apply effect function.
  * 
  * @param array $params 
  * @abstract
  * @access public
  * @return void
  */
 abstract public function applyEfect($params = array());
}
