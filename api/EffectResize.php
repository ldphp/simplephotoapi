<?php
namespace  ImageApi;
require_once 'AEffect.php';
require_once 'IEffect.php';
class EffectResize extends AEffect implements IEffect{
  protected $originalWidth;
  protected $originalHeight;

 /**
  * Set the image instance.
  * 
  * @param mixed $image 
  * @access public
  * @return void
  */
 public function setImage($image, $params = array()){
   $this->originalWidth = $params['width'];
   $this->originalHeight = $params['height'];

   parent::setImage($image, $params);
 }


 /**
  * The resize effect imprementation. 
  * 
  * @param array $params 
  * @access public
  * @return void
  */
 public function applyEfect($params = array()){
     $new_image = imagecreatetruecolor($params['width'], $params['height']);
     imagecopyresampled($new_image, $this->_image, 0, 0, 0, 0, $width, $height, $this->originalWidth, $this->originalHeight);
     return $this->_image;
 }
}
