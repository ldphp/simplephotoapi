<?php
namespace ImageApi;
require_once 'AEffect.php';
require_once 'IEffect.php';
class EffectSharpen extends AEffect implements IEffect{
   
 /**
  * The resize effect imprementation. 
  * 
  * @param array $params 
  * @access public
  * @return void
  */
 public function applyEfect($params = array()){
    $amount= $params['amount'];


    // Amount should be in the range of 18-10
    $amount = round(abs(-18 + ($amount * 0.08)), 2);

    // Gaussian blur matrix
    $matrix = array
        (
         array(-1,   -1,    -1),
         array(-1, $amount, -1),
         array(-1,   -1,    -1),
        );

    // Perform the sharpen
    imageconvolution($this->_image, $matrix, $amount - 8, 0);
     return $this->_image;
 }
}
