<?php
namespace ImageApi;
class ImageApi{
	private static $_app;
	private $_config = array();
	private $_images = array();
	private $_plugins = array();
	
	public static function create(){		
		if (!isset(self::$_app)) {
            $obj = __CLASS__;
            self::$_app = new $obj;
        }
        return self::$_app;
	}
	public static function getVersion()
	{
		return '1.0.0';
	}
	public function init(){
		$this->_config =  require_once(dirname(__FILE__).'/../config.php');
	
		require_once(dirname(__FILE__).'/../api/Image.php');
		
		foreach ($this->_config['Effects'] as $class=>$params){
			if($params['visible']){
				
				
				$ClassPath = str_replace(array('api.','plugins.'),array('api/','plugins/'),$class);
				$ClassPath .=".php";
				
				if(!file_exists(dirname(__FILE__).'/../'.$ClassPath)){
					throw new \Exception("EffectPlugin {$class} dosn;t exist", "404");
				}else {
					
					$this->_plugins[$class]['namespace']  = (strpos($class, "api.")!==false)?"ImageApi":"PluginsImageApi";
					$this->_plugins[$class]['path']  = $ClassPath;
					$this->_plugins[$class]['name']  =  str_replace(array('api.','plugins.'),array('',''),$class);					
					require_once(dirname(__FILE__).'/../'.$ClassPath);
				}
			}
		}
		
	}
	public function savePluginEffects($config){		
		file_put_contents(dirname(__FILE__).'/../config.php',"<?php return ".var_export($config,true).";");
	}
	public function loadPluginEffects(){
		$files = "";
		$efects = array();
		if ($handle = opendir(dirname(__FILE__))) {
		  
		    while (false !== ($file = readdir($handle))) {
		        $files .= "$file\n";
		       
		    }
		  	preg_match("/Effect[a-zA-Z]*.php/", $files,$efects);
		 
		    closedir($handle);
		}
		return $efects;
	}
	public function addImage($img_path,$effects_name=array(),$params_effect=array()){
		$img = new Image($img_path);
		foreach($effects_name as $k=>$effect){
			$objEffect = ImageApi::factory($effect);			
			$objEffect->applyEfect($params_effect[$k]);
			$objEffect->setImage($img);			
		}
		$img->applyEfects();
		$this->_images[basename($img_path)] = $img;
	}
	
	public function saveImages($path){
		foreach($this->_images as $filename=>$img){
			$img->save($path."/".$filename.".jpg");
		}
	}
	
	public static function factory($Efect)
    {
    	if(isset($this->_plugins[$Efect]['namespace']) && isset($this->_plugins[$Efect]['name'])){
    		return new $this->_plugins[$Efect]['namespace'].'/'.$this->_plugins[$Efect]['name'];
    	}else{
    		throw new \Exception("EffectPlugin {$Efect} dosn;t exist", "404");
    	}
    }
	public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
}
